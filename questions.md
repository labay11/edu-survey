# Interview plan

## Before the interview

* The information information about courses the educator teaches is automatically pre-filled from CourseBase
* The interviewer searches online for the CV of the educator, and pre-fills the fields according to the search results

## Introduction

* The interviewer introduces themselves and thanks the educator for agreeing to participate
* The interviewer checks whether the interview has to be conducted in English or Dutch
* The interviewer explains the goal of the project
* The interviewer introduces the project team and mentions its relation to Zesje
* The interviewer explains the policy on retaining the answers and the scope of intended data usage
* The interviewer asks for a permission to record the interview, states the intended use of the recording and storage duration, and starts the recording
> ** CM: What if the educator doesnt give permission?**

## Educator's background

* For how long are you working in the university?
* What is your current job title?
* Are you working part-time, and at what percentage?
* What percentage of your working time is spent on education?
* For how many years have you worked as a course instructor?
* Have you followed the UTQ training or an equivalent training in a different university?
* What courses do you teach? On CourseBase we have found that you teach the following courses, is this information correct?
* What are the largest and the smallest amounts of students enrolled in your course?
* Do you have a special role (formal or informal) in education, such as:
  - Program director
    + How much of your time does this take?
  - Member of an education or exam committee
    + How much of your time does this take?
  - Ad-hoc coordinator of an activity involving multiple courses
    + How much of your time does this take?
    + What activity?

## Educator's current use of educational tools and organizational methods

* What learning activities do you use in your courses? These may include:
  - Lectures
  - Guided exercise classes (somebody solves exercises at the blackboard)
  - Assisted exercise classes (students work individually or in groups, and ask questions from the course team)
  - Homework (graded or ungraded) 
** CM: Do you mean self-paced / self-study?**
    + How do students submit the homework (email, Brightspace, MapleTA, paper, other)
    + How many people grade/check the homework
    + If any, what software is used for grading the homework?
  - Online (self-paced) learning activities? If so, which one(s):
    + Interactive) video
    + Assignments (knowledge check)
    + Discussion board
    + Peer feedback
    + Online quiz
    + Other, please specify
  - Intermediate tests (graded or ungraded)
    + How do you grade these tests?
  - Computer laboratory
    + Do students use their own computers or university-provided ones?
    + What software do you use in this laboratory works?
  - Laboratory using other equipment
  - Office hours
    + Are these organized specially (e.g. with students signing up)?
  - Online communication
* Did you participate in a MOOC?
  - If so, which one?
* What educational materials do you use in your courses? These may include:
  - Books
  - Published lecture notes
  - Video recordings
  - Data-sets
  - Program source code
  - Other, please specify
* Do you know if any of the materials you published were reused outside of the courses you teach?
* How do you share these materials?
  - Are they accessible on brightspace?
  - Are they accessible on open courseware?
  - Do you upload videos to youtube or a similar website?
  - On a separate website?
  - Do you share source files?
  -
* Which methods do you use to assess the students?
  - Hand-written solutions to problems
  - Essays
  - Reports
  - Peer-graded work
  - Discussions
  - Online exams
  - Oral exams
  - Presentation in front of a group
* What materials data do you collect during or after the course?
  - Exam solutions
  -
* What tools do you use to carry out the above activities

## Educator's needs

## Educator's ideas and feedback

## Conclusion and outlook

* The interviewer thanks the educator for the information they provided.
* The interviewer asks if the educator would be interested if the survey team would follow-up on the findings of the survey
* The interviewer proposes a follow-up demonstration of Zesje and asks if the educator would be interested in this
